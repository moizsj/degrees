package degrees;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.MethodSorters;

import twitter4j.TwitterException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestDegreesOfSeparation {
    private static TwitterApiHandlesManager twitterApiHandlesManager;

    @Rule
    public TestWatcher testWatcher = new TestWatcher() {
        @Override
        protected void starting(final Description description) {
            System.out.println("\nstarting test : " + description.getMethodName() + "\n");
        }

        @Override
        protected void succeeded(final Description description) {
            System.out.println("\ntest succeeded : " + description.getMethodName());
        }

        @Override
        protected void failed(final Throwable throwable, final Description description) {
            System.out.println("\ntest failed :" + description.getMethodName());
        }
    };

    @BeforeClass
    public static void createTwitterApiHandles() throws FileNotFoundException, IOException, TwitterException {
        System.out.println("\ndoing pre-test init...\n");
        TestDegreesOfSeparation.twitterApiHandlesManager = new TwitterApiHandlesManager(false);
        System.out.println("\npre-test init done.");
    }

    @Test
    public void testInavlidSource() {
        try {
            new DegreesOfSeparation().computeDegreesOfSeparation("-1", "moizsj", 4, 10, false,
                                                                 TestDegreesOfSeparation.twitterApiHandlesManager);
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof IllegalArgumentException);
        }
    }

    @Test
    public void testInavlidTarget() {
        try {
            new DegreesOfSeparation().computeDegreesOfSeparation("moizsj", "-1", 4, 10, false,
                                                                 TestDegreesOfSeparation.twitterApiHandlesManager);
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof IllegalArgumentException);
        }
    }

    @Test
    public void testDeadEndSource() throws IOException, TwitterException {
        final List<String> acqChain =
                new DegreesOfSeparation().computeDegreesOfSeparation("moizjinia", "moizsj", 4, 10, false,
                                                                     TestDegreesOfSeparation.twitterApiHandlesManager);
        Assert.assertEquals(null, acqChain);
    }

    @Test
    public void test1Degree() throws IOException, TwitterException {
        final List<String> acqChain =
                new DegreesOfSeparation().computeDegreesOfSeparation("moizsj", "mrkwpalmer", 4, 10, false,
                                                                     TestDegreesOfSeparation.twitterApiHandlesManager);
        Assert.assertTrue((acqChain != null) && (acqChain.size() == 2));
    }

    @Test
    public void test2Degrees() throws IOException, TwitterException {
        final List<String> acqChain =
                new DegreesOfSeparation().computeDegreesOfSeparation("moizsj", "mattquinn", 4, 10, false,
                                                                     TestDegreesOfSeparation.twitterApiHandlesManager);
        Assert.assertTrue((acqChain != null) && (acqChain.size() == 3));
    }

    @Test
    public void test3Degrees() throws IOException, TwitterException {
        final List<String> acqChain =
                new DegreesOfSeparation().computeDegreesOfSeparation("moizsj", "BillGates", 4, 10, false,
                                                                     TestDegreesOfSeparation.twitterApiHandlesManager);
        Assert.assertTrue((acqChain != null) && (acqChain.size() == 4));
    }
}
