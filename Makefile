# compile source and create jar
build: clean
	@echo Compiling source...
	@mkdir ./srcproject/bin
	@cp ./srcproject/src/auth.props ./srcproject/bin
	@javac -d ./srcproject/bin/ -classpath ./srcproject/twitter4j-core-4.0.4.jar -source 1.7 -target 1.7 ./srcproject/src/degrees/*.java
	@echo Creating jar in bin...
	@mkdir ./bin
	@cp ./srcproject/input.props ./bin
	@cd ./bin; jar xf ../srcproject/twitter4j-core-4.0.4.jar
	@cd ./bin; jar cfe degrees.jar degrees.DegreesOfSeparation -C ../srcproject/bin . ./twitter4j/ ./META-INF/
	@cd ./bin; rm -rf twitter4j META-INF
	@chmod a+x ./bin/degrees.jar
	@echo Compiling test source...
	@mkdir ./testproject/bin
	@javac -d ./testproject/bin/ -classpath ./testproject/junit-4.12.jar:./bin/degrees.jar -source 1.7 -target 1.7 ./testproject/tests/degrees/*.java
	@echo Done.

#clean all .class files and the jar
clean:
	@echo Deleting all .class files and jar...
	@rm -rf ./bin
	@rm -rf ./srcproject/bin
	@rm -rf ./testproject/bin
	@echo Done.
	
# run junit tests, first check if build needs to be done
ifeq (,$(wildcard bin/degrees.jar))
test: build
	@java -cp ./testproject/bin/:./testproject/junit-4.12.jar:./bin/degrees.jar:./testproject/hamcrest-core-1.3.jar org.junit.runner.JUnitCore degrees.TestDegreesOfSeparation
else
test:
	@java -cp ./testproject/bin/:./testproject/junit-4.12.jar:./bin/degrees.jar:./testproject/hamcrest-core-1.3.jar org.junit.runner.JUnitCore degrees.TestDegreesOfSeparation
endif
	
	
	