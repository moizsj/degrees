package degrees;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

public class DegreesOfSeparation {

    /**
     * @throws IOException
     * @throws TwitterException
     * 
     */
    public List<String> computeDegreesOfSeparation(final String sourceUsername, final String targetUsername,
                                                   final int maxDepth, final int threads, final boolean cycle,
                                                   final TwitterApiHandlesManager... twitterApiHandlesManagers)
            throws IOException, TwitterException {
        TwitterApiHandlesManager twitterApiHandlesManager = null;

        if ((twitterApiHandlesManagers == null) || (twitterApiHandlesManagers.length == 0)) {
            twitterApiHandlesManager = new TwitterApiHandlesManager(cycle);
        } else {
            twitterApiHandlesManager = twitterApiHandlesManagers[0];
        }

        final Twitter twitterApiHandle = twitterApiHandlesManager.getTwitterApiHandle(null);
        if (twitterApiHandle == null) {
            return null;
        }
        final User sourceUser;
        try {
            sourceUser = twitterApiHandle.showUser(sourceUsername);
        } catch (final Exception e) {
            throw new IllegalArgumentException("error while looking up source user " + sourceUsername + " -- " +
                                               e.getMessage());
        }
        final User targetUser;

        try {
            targetUser = twitterApiHandle.showUser(targetUsername);
        } catch (final Exception e) {
            throw new IllegalArgumentException("error while looking up target user " + targetUsername + " -- " +
                                               e.getMessage());
        }

        System.out.println("Determining degrees of separation from --\n" + sourceUser.getName() + " to " +
                           targetUser.getName());

        final long sourceUserId = sourceUser.getId();
        final long targetUserId = targetUser.getId();

        final ForkJoinPool forkJoinPool = new ForkJoinPool(threads);

        final SearchContext searchContext =
                new SearchContext(forkJoinPool, maxDepth, twitterApiHandlesManager,
                                  Collections.newSetFromMap(new ConcurrentHashMap<Long, Boolean>()), targetUserId);

        final TargetFinderRecursiveTask targetFinderRecursiveTask =
                new TargetFinderRecursiveTask(Arrays.asList(new Long[] { sourceUserId }), 1, searchContext);

        final List<Long> acquaintanceChain = forkJoinPool.invoke(targetFinderRecursiveTask);

        if (acquaintanceChain != null) {
            final List<String> acquaintanceChainNames = new ArrayList<String>();
            for (final long userId : acquaintanceChain) {
                acquaintanceChainNames.add(twitterApiHandle.showUser(userId).getName());
            }
            acquaintanceChainNames.add(targetUser.getName());
            return acquaintanceChainNames;
        } else {
            return null;
        }
    }

    public static void main(final String[] args) throws IOException, TwitterException {
        final Properties inputProperties = new Properties();
        inputProperties.load(new FileInputStream("input.props"));

        final String sourceUsername = inputProperties.getProperty("source");
        if ((sourceUsername == null) || (sourceUsername.length() == 0)) {
            throw new IllegalArgumentException("source cannot be empty");
        }

        final String targetUsername = inputProperties.getProperty("target");
        if ((targetUsername == null) || (targetUsername.length() == 0)) {
            throw new IllegalArgumentException("target cannot be empty");
        }

        if (sourceUsername.equals(targetUsername)) {
            System.out.println("source and target are the same, nothing to compute");
            return;
        }

        final int maxDepth = Integer.parseInt(inputProperties.getProperty("maxDepth", "3"));
        if (maxDepth < 1) {
            throw new IllegalArgumentException("maxDepth should be at least 1");
        }

        final int threads = Integer.parseInt(inputProperties.getProperty("threads", "20"));
        if (threads < 1) {
            throw new IllegalArgumentException("threads should be at least 1");
        }
        final boolean cycle = Boolean.parseBoolean(inputProperties.getProperty("cycle", "false"));

        final List<String> acquaintanceChain =
                new DegreesOfSeparation().computeDegreesOfSeparation(sourceUsername, targetUsername, maxDepth, threads,
                                                                     cycle);

        if (acquaintanceChain != null) {
            final int degrees = acquaintanceChain.size() - 1;
            System.out.println("Degrees of Separation = " + degrees);
            System.out.print("Acquaintance Chain = ");
            final Iterator<String> acqChainIter = acquaintanceChain.iterator();
            while (acqChainIter.hasNext()) {
                System.out.print(acqChainIter.next());
                if (acqChainIter.hasNext()) {
                    System.out.print(" --> follows --> ");
                } else {
                    System.out.println();
                }
            }
        } else {
            System.out.println("No connection found.");
        }
    }
}
