package degrees;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterApiHandlesManager {

    final List<Twitter> twitterApiHandles = new LinkedList<Twitter>();
    HashMap<Integer, Integer> availabilityTable = new HashMap<Integer, Integer>();
    int handlePointer = 0;
    boolean cycle = false;

    public TwitterApiHandlesManager(final boolean cycle) throws FileNotFoundException, IOException, TwitterException {
        this.cycle = cycle;
        createTwitterHandles();
        buildAvailabilityTable();
    }

    synchronized Twitter getTwitterApiHandle(final Twitter staleApiHandle) {
        while (true) {
            if (staleApiHandle != null) {
                final int staleIndex = this.twitterApiHandles.indexOf(staleApiHandle);
                this.availabilityTable.put(staleIndex, 0);
            }
            int currentAvailable = this.availabilityTable.get(this.handlePointer);
            if (currentAvailable > 0) {
                this.availabilityTable.put(this.handlePointer, --currentAvailable);
                return this.twitterApiHandles.get(this.handlePointer);
            } else {
                this.handlePointer++;
                if (this.handlePointer == (this.twitterApiHandles.size())) {
                    if (this.cycle) {
                        System.out.println("hit rate limit for all " + this.twitterApiHandles.size() +
                                           " apps, will resume after 16 mins");

                        try {
                            Thread.sleep(16 * 60 * 1000);
                        } catch (final InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.println("done waiting, rebuild availabilty table, and move back to first app");
                        buildAvailabilityTable();
                        this.handlePointer = 0;
                    } else {
                        // rate limit hit
                        System.out.println("hit rate limit for all " + this.twitterApiHandles.size() +
                                           " available apps, quitting.");
                        return null;
                    }
                } else {
                    System.out.println("app " + this.handlePointer + " quota exhausted, now using app " +
                                       (this.handlePointer + 1));
                }
            }
        }
    }

    private void buildAvailabilityTable() {
        System.out.println("checking query limits...");
        int totalAvailable = 0;
        for (int i = 0; i < this.twitterApiHandles.size(); i++) {
            RateLimitStatus rateLimitStatus = null;
            try {
                rateLimitStatus = this.twitterApiHandles.get(i).getRateLimitStatus().get("/friends/ids");
            } catch (final TwitterException e) {
            }
            int available = 0;
            if (rateLimitStatus != null) {
                available = rateLimitStatus.getRemaining();
            }
            this.availabilityTable.put(i, available);
            totalAvailable += available;
            System.out.println(available + " remaining for app " + (i + 1));
        }
        System.out.println("total queries available = " + totalAvailable);
    }

    private void createTwitterHandles() throws IOException, TwitterException {
        System.out.print("setting up auth for Twitter apps");
        final Properties authValuesProps = new Properties();
        authValuesProps.load(this.getClass().getClassLoader().getResourceAsStream("auth.props"));

        for (int i = 1; i <= authValuesProps.size(); i++) {
            final String[] authValues = authValuesProps.getProperty(Integer.toString(i)).split(",");

            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setApplicationOnlyAuthEnabled(true);
            builder.setOAuthConsumerKey(authValues[0]);
            builder.setOAuthConsumerSecret(authValues[1]);
            final Twitter twitterApiHandle = new TwitterFactory(builder.build()).getInstance();
            twitterApiHandle.getOAuth2Token();

            this.twitterApiHandles.add(twitterApiHandle);
            System.out.print(".");
        }
        System.out.println("done");
    }
}
