package degrees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

class TargetFinderRecursiveTask extends RecursiveTask<List<Long>> {

    private static final long serialVersionUID = 1L;

    final List<Long> sourceUserIds;
    final int level;
    final SearchContext searchContext;

    public TargetFinderRecursiveTask(final List<Long> sourceUserIds, final int level, final SearchContext searchContext) {
        super();
        this.sourceUserIds = sourceUserIds;
        this.level = level;
        this.searchContext = searchContext;
    }

    @Override
    protected List<Long> compute() {
        if (this.searchContext.getForkJoinPool().isShutdown()) {
            return null;
        }

        long[] follows = null;
        final long sourceUserId = this.sourceUserIds.get(this.sourceUserIds.size() - 1);

        Twitter staleApiHandle = null;
        while (true) {
            final Twitter twitterApiHandle =
                    this.searchContext.getTwiiterApiHandlesManager().getTwitterApiHandle(staleApiHandle);
            if (twitterApiHandle == null) {
                // out of handles
                this.searchContext.getForkJoinPool().shutdown();
                return null;
            }

            try {
                follows = twitterApiHandle.getFriendsIDs(sourceUserId, -1).getIDs();
                break;
            } catch (final TwitterException e) {
                // check for protected user
                final int statusCode = e.getStatusCode();
                final String message = e.getMessage();

                if (statusCode == 429) {
                    System.out.println("hit rate limit inspite of check, twitter API returned incorrect results.");
                    staleApiHandle = twitterApiHandle;
                    continue;
                }

                User user = null;
                try {
                    user = twitterApiHandle.showUser(sourceUserId);
                } catch (final TwitterException e2) {
                }
                if ((statusCode == 401) && message.toLowerCase().contains("not authorized")) {
                    System.out.println("Ran into protected user " + (user != null ? user.getName() : "") +
                                       ", cannot access data, skipping.");
                } else {
                    System.out.println("Ran into error '" + message + "' while querying user " +
                                       (user != null ? user.getName() : "") + ", cannot access data, skipping.");
                }
                return null;
            }
        }

        List<Long> resultSourceIds = null;
        boolean found = false;
        for (int i = 0; i < follows.length; i++) {
            if (follows[i] == this.searchContext.getTargetUserId()) {
                // found match
                found = true;
                resultSourceIds = this.sourceUserIds;
                this.searchContext.getForkJoinPool().shutdown();
            }
        }

        if (!found && !this.searchContext.getForkJoinPool().isShutdown() &&
            ((this.level + 1) <= this.searchContext.getMaxDegrees())) {
            // System.out.println("not found at level " + this.level);
            if ((follows != null) && (follows.length > 0)) {

                // create subtasks
                final List<TargetFinderRecursiveTask> subTasks = new ArrayList<TargetFinderRecursiveTask>();
                for (int i = 0; i < follows.length; i++) {
                    if (!this.searchContext.getProcessedUserIds().contains(follows[i])) {
                        // attempt to add to set. add is atomic since the set is backed by a concurrentmap
                        // proceed only if add succeeds
                        if (this.searchContext.getProcessedUserIds().add(follows[i])) {
                            final List<Long> newSourceUserIds = new LinkedList<Long>();
                            newSourceUserIds.addAll(this.sourceUserIds);
                            newSourceUserIds.add(follows[i]);
                            subTasks.add(new TargetFinderRecursiveTask(newSourceUserIds, this.level + 1,
                                                                       this.searchContext));
                        }
                    }
                }

                if (this.searchContext.getForkJoinPool().isShutdown()) {
                    return null;
                }

                // fork all subtasks
                for (final TargetFinderRecursiveTask targetFinderRecursiveTask : subTasks) {
                    targetFinderRecursiveTask.fork();
                }

                // join all subtasks
                for (final TargetFinderRecursiveTask targetFinderRecursiveTask : subTasks) {
                    final List<Long> resultSubTaskSourceIds = targetFinderRecursiveTask.join();
                    if (resultSubTaskSourceIds != null) {
                        if (resultSourceIds == null) {
                            resultSourceIds = resultSubTaskSourceIds;
                        } else if (resultSubTaskSourceIds.size() < resultSourceIds.size()) {
                            resultSourceIds = resultSubTaskSourceIds;
                        }
                    }
                }
            }
        }
        return resultSourceIds;
    }
}
