package degrees;

import java.util.Set;
import java.util.concurrent.ForkJoinPool;

class SearchContext {
    final ForkJoinPool forkJoinPool;
    final int maxDegrees;
    final TwitterApiHandlesManager twiiterApiHandlesManager;
    final Set<Long> processedUserIds;
    final long targetUserId;

    /**
     * @param forkJoinPool
     * @param maxDegrees
     * @param twiiterApiHandlesManager
     * @param processedUserIds
     * @param targetUserId
     */
    public SearchContext(final ForkJoinPool forkJoinPool, final int maxDegrees,
            final TwitterApiHandlesManager twiiterApiHandlesManager, final Set<Long> processedUserIds,
            final long targetUserId) {
        super();
        this.forkJoinPool = forkJoinPool;
        this.maxDegrees = maxDegrees;
        this.twiiterApiHandlesManager = twiiterApiHandlesManager;
        this.processedUserIds = processedUserIds;
        this.targetUserId = targetUserId;
    }

    /**
     * @return the forkJoinPool
     */
    public ForkJoinPool getForkJoinPool() {
        return this.forkJoinPool;
    }

    /**
     * @return the maxDegrees
     */
    public int getMaxDegrees() {
        return this.maxDegrees;
    }

    /**
     * @return the twiiterApiHandlesManager
     */
    public TwitterApiHandlesManager getTwiiterApiHandlesManager() {
        return this.twiiterApiHandlesManager;
    }

    /**
     * @return the processedUserIds
     */
    public Set<Long> getProcessedUserIds() {
        return this.processedUserIds;
    }

    /**
     * @return the targetUserId
     */
    public long getTargetUserId() {
        return this.targetUserId;
    }

}
